﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppSekolah.Data;
using WebAppSekolah.Data.Models;
using WebAppSekolah.Models;

namespace WebAppSekolah.Controllers
{
    public class SiswaController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SiswaController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Siswa
        public async Task<IActionResult> Index()
        {
            var getData = (from a in _context.MS_Siswa
                           join b in _context.MS_Kelas on a.kelasID equals b.Id
                           select new SiswaViewModel
                           {
                               Id = a.Id,
                               NIM = a.NIM,
                               Nama = a.nama,
                               KelasID = a.kelasID,
                               Kelas = b.tingkat +"-"+ b.jurusan
                           }).ToListAsync();

            return View(await getData);
        }

        // GET: Siswa/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siswaViewModel = (from a in _context.MS_Siswa
                                  join b in _context.MS_Kelas on a.kelasID equals b.Id
                                  where a.Id == id
                                  select new SiswaViewModel
                                  {
                                      Id = a.Id,
                                      NIM = a.NIM,
                                      Nama = a.nama,
                                      KelasID = a.kelasID,
                                      Kelas = b.tingkat + "-" + b.jurusan
                                  }).FirstOrDefaultAsync();

            if (siswaViewModel == null)
            {
                return NotFound();
            }

            return View(await siswaViewModel);
        }

        // GET: Siswa/Create
        public IActionResult Create()
        {
            var listKelas = (from a in _context.MS_Kelas
                             select new
                             {
                                 KelasID = a.Id,
                                 Kelas = a.tingkat + "-" + a.jurusan
                             }).ToList();

            ViewBag.ListOfKelas = listKelas;
            return View();
        }

        // POST: Siswa/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NIM,Nama,Kelas,KelasID")] SiswaViewModel siswaViewModel)
        {
            if (ModelState.IsValid)
            {
                var dataSiswa = new MS_Siswa
                {
                    NIM = siswaViewModel.NIM,
                    nama = siswaViewModel.Nama,
                    kelasID = siswaViewModel.KelasID
                };

                _context.MS_Siswa.Add(dataSiswa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(siswaViewModel);
        }

        // GET: Siswa/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siswaViewModel = (from a in _context.MS_Siswa
                                  join b in _context.MS_Kelas on a.kelasID equals b.Id
                                  where a.Id == id
                                  select new SiswaViewModel
                                  {
                                      Id = a.Id,
                                      NIM = a.NIM,
                                      Nama = a.nama,
                                      KelasID = a.kelasID,
                                      Kelas = b.tingkat + "-" + b.jurusan
                                  }).FirstOrDefaultAsync();

            if (siswaViewModel == null)
            {
                return NotFound();
            }

            var listKelas = (from a in _context.MS_Kelas
                             select new
                             {
                                 KelasID = a.Id,
                                 Kelas = a.tingkat + "-" + a.jurusan
                             }).ToListAsync();

            ViewBag.ListOfKelas = await listKelas;

            return View(await siswaViewModel);
        }

        // POST: Siswa/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NIM,Nama,Kelas,KelasID")] SiswaViewModel siswaViewModel)
        {
            if (id != siswaViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var dataSiswa = new MS_Siswa
                    {
                        Id = siswaViewModel.Id,
                        NIM = siswaViewModel.NIM,
                        nama = siswaViewModel.Nama,
                        kelasID = siswaViewModel.KelasID
                    };

                    _context.MS_Siswa.Update(dataSiswa);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SiswaExists(siswaViewModel.NIM, siswaViewModel.Nama, siswaViewModel.KelasID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(siswaViewModel);
        }

        // GET: Siswa/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var siswaViewModel = (from a in _context.MS_Siswa
                                  join b in _context.MS_Kelas on a.kelasID equals b.Id
                                  where a.Id == id
                                  select new SiswaViewModel
                                  {
                                      Id = a.Id,
                                      NIM = a.NIM,
                                      Nama = a.nama,
                                      KelasID = a.kelasID,
                                      Kelas = b.tingkat + "-" + b.jurusan
                                  }).FirstOrDefaultAsync();
            if (siswaViewModel == null)
            {
                return NotFound();
            }

            return View(await siswaViewModel);
        }

        // POST: Siswa/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var siswaViewModel = await _context.MS_Siswa.SingleOrDefaultAsync(m => m.Id == id);
            _context.MS_Siswa.Remove(siswaViewModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SiswaExists(string NIM, string nama, int kelasID)
        {
            return _context.MS_Siswa.Any(e => e.NIM == NIM && e.nama == nama && e.kelasID == kelasID);
        }
    }
}
