﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAppSekolah.Data;
using WebAppSekolah.Models;

namespace WebAppSekolah.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            var dataSiswa = (from a in _context.MS_Siswa
                             group a by a.kelasID into g
                             select new { kelasID = g.Key, count = g.Count() }).ToList();
            var dataPoints = (from a in _context.MS_Kelas
                              join b in dataSiswa on a.Id equals b.kelasID into bb
                              from b in bb.DefaultIfEmpty()
                              select new DataPoint(a.tingkat + "-" + a.jurusan, b.count)).ToList();

            var getAllSiswa = (from a in _context.MS_Siswa
                               select a).Count();

            var getAllKelas = (from a in _context.MS_Kelas
                               select a).Count();

            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            ViewBag.TotalSiswa = getAllSiswa;
            ViewBag.TotalKelas = getAllKelas;

            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
