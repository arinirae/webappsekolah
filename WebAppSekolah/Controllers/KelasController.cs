﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebAppSekolah.Data;
using WebAppSekolah.Data.Models;
using WebAppSekolah.Models;

namespace WebAppSekolah.Controllers
{
    public class KelasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public KelasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Kelas
        public async Task<IActionResult> Index()
        {
            var getData = (from a in _context.MS_Kelas
                           select new KelasViewModel
                           {
                               Id = a.Id,
                               Tingkat = a.tingkat,
                               Jurusan = a.jurusan,
                               WaliKelas = a.waliKelas
                           }).ToListAsync();

            return View(await getData);
        }

        // GET: Kelas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kelasViewModel = await _context.MS_Kelas.Select(x => new KelasViewModel
                                {
                                    Id = x.Id,
                                    Tingkat = x.tingkat,
                                    Jurusan = x.jurusan,
                                    WaliKelas = x.waliKelas
                                }).SingleOrDefaultAsync(m => m.Id == id);
            if (kelasViewModel == null)
            {
                return NotFound();
            }

            return View(kelasViewModel);
        }

        // GET: Kelas/Create
        public IActionResult Create()
        {
            var jurusan = new[] { "IPA", "IPS" };

            var listJurusan = jurusan.Select(x => new
                               {
                                   Jurusan = x
                               }).ToList();

            ViewBag.ListOfJurusan = listJurusan;
            return View();
        }

        // POST: Kelas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Tingkat,Jurusan,WaliKelas")] KelasViewModel kelasViewModel)
        {
            if (ModelState.IsValid)
            {
                var data = new MS_Kelas
                {
                    tingkat = kelasViewModel.Tingkat,
                    jurusan = kelasViewModel.Jurusan,
                    waliKelas = kelasViewModel.WaliKelas
                };

                _context.MS_Kelas.Add(data);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(kelasViewModel);
        }

        // GET: Kelas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kelasViewModel = await _context.MS_Kelas.Select(x => new KelasViewModel {
                                    Id = x.Id,
                                    Tingkat = x.tingkat,
                                    Jurusan = x.jurusan,
                                    WaliKelas = x.waliKelas
                                }).SingleOrDefaultAsync(m => m.Id == id);
            if (kelasViewModel == null)
            {
                return NotFound();
            }

            var jurusan = new[] { "IPA", "IPS" };

            var listJurusan = jurusan.Select(x => new
            {
                Jurusan = x
            }).ToList();

            ViewBag.ListOfJurusan = listJurusan;

            return View(kelasViewModel);
        }

        // POST: Kelas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Tingkat,Jurusan,WaliKelas")] KelasViewModel kelasViewModel)
        {
            if (id != kelasViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var kelas = new MS_Kelas
                    {
                        Id = kelasViewModel.Id,
                        tingkat = kelasViewModel.Tingkat,
                        jurusan = kelasViewModel.Jurusan,
                        waliKelas = kelasViewModel.WaliKelas
                    };

                    _context.MS_Kelas.Update(kelas);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!KelasExists(kelasViewModel.Tingkat, kelasViewModel.Jurusan,kelasViewModel.WaliKelas))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(kelasViewModel);
        }

        // GET: Kelas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var kelasViewModel = await _context.MS_Kelas.Select(x => new KelasViewModel
                                {
                                    Id = x.Id,
                                    Tingkat = x.tingkat,
                                    Jurusan = x.jurusan,
                                    WaliKelas = x.waliKelas
                                }).SingleOrDefaultAsync(m => m.Id == id);
            if (kelasViewModel == null)
            {
                return NotFound();
            }

            return View(kelasViewModel);
        }

        // POST: Kelas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var getKelas = await _context.MS_Kelas.SingleOrDefaultAsync(m => m.Id == id);
            _context.MS_Kelas.Remove(getKelas);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool KelasExists(string tingkat, string jurusan, string waliKelas)
        {
            return _context.MS_Kelas.Any(e => e.tingkat == tingkat && e.jurusan == jurusan && e.waliKelas == waliKelas);
        }
    }
}
