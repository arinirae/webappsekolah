﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppSekolah.Models
{
    public class KelasViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Tingkat")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Tingkat { get; set; }

        [Required]
        [Display(Name = "Jurusan")]
        [StringLength(10, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Jurusan { get; set; }

        [Required]
        [Display(Name = "Wali Kelas")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string WaliKelas { get; set; }
    }
}
