﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppSekolah.Models
{
    public class SiswaViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "NIM")]
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string NIM { get; set; }

        [Required]
        [Display(Name = "Nama")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 1)]
        public string Nama { get; set; }
        
        [Display(Name = "Kelas")]
        public string Kelas { get; set; }

        public int KelasID { get; set; }
    }
}
