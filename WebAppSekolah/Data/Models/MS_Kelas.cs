﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppSekolah.Data.Models
{
    public class MS_Kelas
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string tingkat { get; set; }

        [Required]
        [StringLength(10)]
        public string jurusan { get; set; }

        [Required]
        [StringLength(50)]
        public string waliKelas { get; set; }

        public ICollection<MS_Siswa> MS_Siswa { get; set; }
    }
}
