﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppSekolah.Data.Models
{
    public class MS_Siswa
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string NIM { get; set; }

        [Required]
        [StringLength(50)]
        public string nama { get; set; }

        [ForeignKey("MS_Kelas")]
        public int kelasID { get; set; }
        public virtual MS_Kelas MS_Kelas { get; set; }

    }
}
